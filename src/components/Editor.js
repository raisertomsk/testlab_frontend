import React, { Component } from 'react';
import { FormControl, FormGroup, Button, ControlLabel } from 'react-bootstrap';
import { NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Redirect } from 'react-router-dom'

class Editor extends Component {


    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            firstName: '',
            lastName: '',
            patronymicName: '',
            phone: '',
            email: '',
            id: '',
            redirect: false
        }
        var id = this.props.location.pathname.split('/');
        if (id[2]) {
            this.state.id = id[2];
        }

        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        var url = "http://testlab.local/api/create";
        if (this.state.id) {
            url = "http://testlab.local/api/update?id=" + this.state.id;
        }
        fetch(url, {
            method: "POST",
            body: JSON.stringify(this.state)
        })
            .then(response => response.json())
            .then((response) => {
                if (response.error) {
                    this.setState({
                        error: response.error
                    });
                } else {
                    this.setState({ redirect: true });
                }
            });
        return false;
    }

    onChangeHandler(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    componentWillMount() {
        this.setState({
            loaded: false
        });
        if (this.state.id) {
            fetch(`http://testlab.local/api/show?id=${this.state.id}`)
                .then(response => response.json())
                .then((response) => {
                    if (!response.error) {
                        for (var i in response) {
                            if (!response[i]) {
                                response[i] = ''
                            }
                        }
                        response.id = response.idLine
                        this.setState(response);
                    }
                    this.setState({
                        loaded: true
                    });
                });
        } else {
            this.setState({
                loaded: true
            });
        }
    }

    render() {
        if (!this.state.loaded) {
            return null;
        }
        return (
            <div className="editForm">
                {this.renderRedirect()}
                <LinkContainer to={"/"}><NavItem>Index</NavItem></LinkContainer>
                <div>
                    <h3>User Editor</h3>
                    <strong>{this.state.error}</strong>
                    <form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <ControlLabel>First Name</ControlLabel>
                            <FormControl
                                name="firstName"
                                type="text"
                                placeholder="Enter First Name"
                                value={this.state.firstName}
                                onChange={this.onChangeHandler}
                            />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Last Name *</ControlLabel>
                            <FormControl
                                name="lastName"
                                type="text"
                                placeholder="Enter Last Name"
                                value={this.state.lastName}
                                onChange={this.onChangeHandler}
                            />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Patronymic Name</ControlLabel>
                            <FormControl
                                name="patronymicName"
                                type="text"
                                placeholder="Enter Patronymic Name"
                                value={this.state.patronymicName}
                                onChange={this.onChangeHandler}
                            />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Phone *</ControlLabel>
                            <FormControl
                                name="phone"
                                type="text"
                                placeholder="Enter Phone"
                                value={this.state.phone}
                                onChange={this.onChangeHandler}
                            />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Email *</ControlLabel>
                            <FormControl
                                name="email"
                                type="text"
                                placeholder="Enter Email"
                                value={this.state.email}
                                onChange={this.onChangeHandler}
                            />
                        </FormGroup>
                        <Button type="submit">Submit</Button>
                    </form>
                </div>
            </div>
        );
    }

    renderRedirect() {
        if (this.state.redirect) {
            return <Redirect to='/' />
        }
    }

}

export default Editor;
