import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { Link } from 'react-router-dom';

class RecordTable extends Component {

    constructor(props) {
        super(props);

        this.onTableParamsChange = this.onTableParamsChange.bind(this);
        this.onDeleteAction = this.onDeleteAction.bind(this);

        this.state = {
            data: [],
            pages: -1,
            deleted: ''
        };

        this.tableColumns = [{
            Header: 'Records',
            columns: [
                {
                    Header: 'ID',
                    accessor: 'id'
                },
                {
                    Header: 'First Name',
                    accessor: 'firstName'
                },
                {
                    Header: 'Last Name',
                    accessor: 'lastName'
                },
                {
                    Header: 'Patronymic Name',
                    accessor: 'patronymicName'
                },
                {
                    Header: 'Phone',
                    accessor: 'phone'
                },
                {
                    Header: 'Email',
                    accessor: 'email'
                },
                {
                    Header: 'Links',
                    id: 'links',
                    Cell: (cell) => {
                        return (
                            <div>
                                <Link to={`/edit/${cell.row.id}`}>Edit</Link><br />
                                <a href="/" id={cell.row.id}
                                    onClick={this.onDeleteAction}
                                >Remove</a>
                            </div>
                        );
                    }
                }
            ],
        }];

    }

    onDeleteAction(event) {
        event.persist();
        event.preventDefault();
        fetch('http://testlab.local/api/delete?id=' + event.target.id)
            .then(response => response.json())
            .then((response) => {
                this.onTableParamsChange(this.state);
            });
    }

    onTableParamsChange(state) {
        fetch('http://testlab.local/api/index', {
            method: "POST",
            body: JSON.stringify({
                from: state.page * state.pageSize,
                quantity: state.pageSize
            })
        })
            .then(response => response.json())
            .then((response) => {
                this.setState({
                    data: this.responseToTableData(response.response),
                    pages: parseInt(response.pagesCount, 10)
                });
            });
    }

    responseToTableData(elements) {
        return Object.keys(elements).map(value => ({
            id: elements[value].idLine,
            firstName: elements[value].firstName,
            lastName: elements[value].lastName,
            patronymicName: elements[value].patronymicName,
            email: elements[value].email,
            phone: elements[value].phone
        }));
    }

    render() {
        const { data } = this.state;
        return (
            <div>
                <ReactTable
                    manual
                    pages={this.state.pages}
                    data={data}
                    columns={this.tableColumns}
                    defaultPageSize={10}
                    className="-striped -highlight"
                    onFetchData={this.onTableParamsChange}
                />
                <br />
            </div>
        );
    }

}

export default RecordTable;
