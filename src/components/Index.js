import React, { Component } from 'react';
import RecordTable from './RecordTable';
import { NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class Index extends Component {
    render() {
        return (
            <div>
                <LinkContainer to={"/edit"}><NavItem>Add</NavItem></LinkContainer>
                <RecordTable />
            </div>
        );
    }
}

export default Index;
