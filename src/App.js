import React, { Component } from 'react';
import './App.css';
import { Switch, Route } from 'react-router';
import Editor from './components/Editor';
import Index from './components/Index';

class App extends Component {
    render() {
        return (
            <div className="App">
                <Switch>
                    <Route exact path='/' component={Index} />
                    <Route path='/edit' component={Editor} />
                </Switch>
            </div>
        );
    }
}

export default App;
